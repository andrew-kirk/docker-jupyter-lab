FROM amazonlinux:latest

#############################
# INSTALL SYSTEM PACKAGES
#############################

# Install required system packages
RUN  yum -y update
RUN yum install -y gcc git curl make tar zlib-devel bzip2 bzip2-devel readline-devel sqlite \
sqlite-devel openssl-devel xz xz-devel libffi-devel findutils which

#############################
# APP USER SETUP
#############################

# Create and use an app user
RUN groupadd --system --gid 1000 app \
 && useradd --uid 1000 --no-log-init --shell /bin/bash --system --create-home --home /home/app --gid app app \
 && chmod 755 /home/app

USER 1000

ENV HOME="/home/app"
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

WORKDIR $HOME
SHELL ["/bin/bash", "-c"]

#############################
# PYTHON INSTALLATION
#############################

ARG python_version=3.8.5

# Install pyenv
RUN git clone https://github.com/pyenv/pyenv.git $HOME/.pyenv
ENV PYENV_ROOT="$HOME/.pyenv"
ENV PATH="$PYENV_ROOT/bin:$PATH"

# Enable shims and autocompletion by adding pyenv init to the shell
RUN echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> $HOME/.bashrc

# Install Python
RUN pyenv install $python_version && pyenv global $python_version && pyenv rehash

## Install/setup pipenv
RUN eval "$(pyenv init -)" && curl https://raw.githubusercontent.com/pypa/pipenv/master/get-pipenv.py | python

#############################
# NODE INSTALLATION
#############################

ARG node_version=12.16.1

RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
RUN source .bashrc && command -v nvm

RUN source .bashrc && nvm install $node_version && nvm use $node_version

#############################
# APP INSTALLATION
#############################

# Setup Python environment
COPY --chown=1000:1000 Pipfile .
RUN source .bashrc && pipenv --python $python_version
RUN source .bashrc && pipenv install

# Jupyter Lab
RUN mkdir $HOME/.jupyter
COPY --chown=1000:1000 configuration/jupyter_notebook_config.py $HOME/.jupyter/jupyter_notebook_config.py

RUN source .bashrc \
    && pipenv run jupyter labextension install @jupyter-widgets/jupyterlab-manager@1.1 \
        @bokeh/jupyter_bokeh@1.1.0 \
    && pipenv run jupyter labextension list


# Copy files last so that changes don't require rebuilding previous build steps
COPY --chown=1000:1000 configuration $HOME/configuration/.
COPY --chown=1000:1000 notebooks/ $HOME/notebooks/.
COPY --chown=1000:1000 supervisord.conf .
RUN mkdir /home/app/logs

EXPOSE 8888

CMD source .bashrc && pipenv run supervisord -c supervisord.conf
