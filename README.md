# JupyterLab Docker Image
## Overview
A [Docker](https://www.docker.com/why-docker) image for running a JupyterLab instance, with an IPython kernel.

## Usage

#### Build
To build and tag the image locally:
 
```sh
docker build --tag local/lab:latest .
```

* The Python version to be used in the container can be specified using the `python_version` build argument
* The NodeJs version to be used in the container (for extensions) can be specified using the `node_version` build argument 

#### Run
To run the docker image locally:
```sh
docker run --name lab --init --publish 8888:8888 --rm local/lab:latest
```

To run the docker image and use the current working directory on the host machine:
```sh
docker run /
  --name lab /
  --init /
  --publish 8888:8888 /
  --mount type=bind,source="$(PWD)",target=/home/app/notebooks  
  --rm 
  local/lab:latest
```

To open an interactive shell as the root user:
```sh
docker run -it --rm --user root local/lab:latest bash
```

To open an interactive shell as the app user:
```sh
docker run -it --rm --user app local/lab:latest bash
```

#### Running Additional Processes
[Supervisor](http://supervisord.org/index.html) is used to start the JupyterLab instance. If additional processes are needed, these can also be added to `suppoervisord.conf` 

`supervisord` is run as the "app" user.

